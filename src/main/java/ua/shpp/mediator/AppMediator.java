package ua.shpp.mediator;

import ua.shpp.abstracts.ChatInterface;
import ua.shpp.abstracts.AbstractUser;
import ua.shpp.classes.Administrator;
import ua.shpp.classes.TextChat;
import ua.shpp.classes.User;

public class AppMediator {
    public static void main(String[] args) {
        ChatInterface textChat = new TextChat();
        AbstractUser administrator = new Administrator(textChat, "Olena");
        AbstractUser user1 = new User(textChat, "Tim");
        AbstractUser user2 = new User(textChat, "Aleks");
        AbstractUser user3 = new User(textChat, "Max");

        textChat.addAdmin(administrator);
        textChat.addUser(user1);
        textChat.addUser(user2);
        textChat.addUser(user3);

        user1.setEnable(false);


        user1.sendMessage("Hello");
        user2.sendMessage("Hi");
        user3.sendMessage("Good evening");
        administrator.sendMessage("Quiet!");
    }
}