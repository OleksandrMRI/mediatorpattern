package ua.shpp.abstracts;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import ua.shpp.classes.User;

import java.util.Objects;

public abstract class AbstractUser {
    Logger log = LoggerFactory.getLogger(AbstractUser.class);
    private String name;
    private ChatInterface chat;
    boolean enable = true;

    public boolean isEnable() {
        return enable;
    }
    public void  setEnable(boolean enable){
        this.enable = enable;
    }

    public AbstractUser(ChatInterface chat, String name) {
        this.chat = chat;
        this.name = name;
    }

    public void sendMessage(String message) {
        chat.sendMessage(message, this);
    }
    public String getName() {
        return name;
    }

    public abstract void getMessage(String message, String consumer, String sender);

}

