package ua.shpp.abstracts;

public interface ChatInterface {
    void sendMessage(String message, AbstractUser user);
    void addUser(AbstractUser userInterface);
    void addAdmin(AbstractUser userInterface);
}
