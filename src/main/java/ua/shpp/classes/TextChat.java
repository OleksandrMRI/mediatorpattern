package ua.shpp.classes;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import ua.shpp.abstracts.ChatInterface;
import ua.shpp.abstracts.AbstractUser;

import java.util.ArrayList;
import java.util.List;

public class TextChat implements ChatInterface {
    Logger log = LoggerFactory.getLogger(TextChat.class);
    AbstractUser administrator;
    List<AbstractUser> users = new ArrayList<>();

    @Override
    public void sendMessage(String message, AbstractUser user) {
        for (AbstractUser u : users) {
            if (u != user && u.isEnable() || administrator == user) {
                u.getMessage(message, u.getName(), user.getName());
            }
        }
        if (administrator != user) {
            administrator.getMessage(message, administrator.getName(), user.getName());
        }
    }

    public void addAdmin(AbstractUser userInterface) {
        this.administrator = userInterface;
    }

    public void addUser(AbstractUser userInterface) {
        users.add(userInterface);
    }
}
