package ua.shpp.classes;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import ua.shpp.abstracts.ChatInterface;
import ua.shpp.abstracts.AbstractUser;

public class User extends AbstractUser {
    Logger log = LoggerFactory.getLogger(User.class);

    public User(ChatInterface chat, String name) {
        super(chat, name);
    }



    @Override
    public void getMessage(String message, String consumer, String sender) {
        log.info("User {} gets message: {}, from {}.",consumer, message,sender);
    }

}
