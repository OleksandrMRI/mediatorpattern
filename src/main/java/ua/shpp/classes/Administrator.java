package ua.shpp.classes;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import ua.shpp.abstracts.ChatInterface;
import ua.shpp.abstracts.AbstractUser;

public class Administrator extends AbstractUser {
    private final Logger log = LoggerFactory.getLogger(Administrator.class);

    public Administrator(ChatInterface chat, String name) {
        super(chat, name);
    }


    @Override
    public void getMessage(String message, String consumer, String sender) {
        log.info("Administrator {} gets message: {}, from {}.", consumer, message, sender);
    }


}
